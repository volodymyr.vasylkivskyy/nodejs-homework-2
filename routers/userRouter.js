const express = require('express');
const router = new express.Router();
const {authMiddleWare} = require('./middlewares/authMiddleWare');
const {passValid} = require('./middlewares/validationMiddleware');
const {
  getUser,
  changePassword,
  deleteUser,
} = require('../controllers/userController');

router.get('/', authMiddleWare, getUser);

router.patch('/', authMiddleWare, passValid, changePassword);

router.delete('/', authMiddleWare, deleteUser);

module.exports = router;
