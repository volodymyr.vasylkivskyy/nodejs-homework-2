const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config.js');

module.exports.authMiddleWare = async (req, res, next) => {
  // const jwtToken = req.header['authorization'];

  // if (!jwtToken) {
  //   return res.status(400).json({ message: `No Authorization http header` });
  // }

  // await jwt.verify(jwtToken, JWT_SECRET, (err, data) => {
  //   if (err) {
  //     return res.status(400).json({ message: 'No valid JWT!' });
  //   }

  //   req.user = data;
  //   next();
  // });
  const reqToken = req.header('Authorization');
  if (!reqToken) {
    return res.status(400).json({message: 'Bad request'});
  }
  const [tokenType, token] = reqToken.split(' ');
  if (tokenType !== 'JWT') {
    return res.status(400).json({message: 'Bad request'});
  }
  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    req.user = decoded;
    next();
  } catch (error) {
    return res.status(400).json({message: 'Bad request'});
  }
};
