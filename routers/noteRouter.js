const express = require('express');
const router = new express.Router();
const {authMiddleWare} = require('./middlewares/authMiddleWare');
const {
  createNote,
  getNotes,
  getNoteId,
  updateNote,
  modifyNote,
  deleteNote,
} = require('../controllers/noteController');

router.post('/', authMiddleWare, createNote);

router.get('/', authMiddleWare, getNotes);

router.get('/:id', authMiddleWare, getNoteId);

router.put('/:id', authMiddleWare, updateNote);

router.patch('/:id', authMiddleWare, modifyNote);

router.delete('/:id', authMiddleWare, deleteNote);

module.exports = router;
