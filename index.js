require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const app = express();

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');

const {PORT} = require('./config');
const {startDB} = require('./server');

app.use(express.json());
app.use(morgan('combined'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/notes', noteRouter);

app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});

startDB();
app.listen(PORT, () => {
  console.log(`server is working at port ${PORT}`);
});
