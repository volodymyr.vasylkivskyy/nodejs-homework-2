const {DB_USER, DB_PASS, DB_NAME, DB_HOSTNAME} = require('./config');
const mongoose = require('mongoose');

module.exports.startDB = async () => {
  await mongoose.connect(
      `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOSTNAME}/${DB_NAME}?retryWrites=true&w=majority`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );
};
