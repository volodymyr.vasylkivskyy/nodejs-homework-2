const {Note} = require('../models/noteModel');

/**
 * Create Note
 * @param {object} req
 * @param {object} res
 */
async function createNote(req, res) {
  try {
    const {text} = req.body;
    const note = new Note({
      userId: req.user._id,
      completed: false,
      text: text,
    });
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
}

/**
 * Get Note
 * @param {obj} req
 * @param {obj} res
 */
async function getNotes(req, res) {
  try {
    const {offset = 0, limit = 5} = req.query;
    const userNotes = await Note.find(
        {userId: req.user._id},
        {__v: 0},
        {
          skip: parseInt(offset),
          limit: limit > 100 ? 5 : parseInt(limit),
        },
    );
    res.status(200).json({notes: userNotes});
  } catch (error) {
    res.status(400).json({message: error.mesage});
  }
}

/**
 * Get Note By Id
 * @param {obj} req
 * @param {obj} res
 */
async function getNoteId(req, res) {
  try {
    const userNote = await Note.findOne({
      userId: req.user._id,
      _id: req.params.id,
    });
    res.status(200).json({note: userNote});
  } catch (error) {
    res.status(400).json({message: error.message});
  }
}

/**
 * Update Note
 * @param {obj} req
 * @param {obj} res
 */
async function updateNote(req, res) {
  try {
    await Note.updateOne(
        {userId: req.user._id, _id: req.params.id},
        {text: req.body.text},
    );
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: error.message});
  }
}

/**
 * Modify
 * @param {obj} req
 * @param {obj} res
 */
async function modifyNote(req, res) {
  try {
    const notes = await Note.find({userId: req.user, _id: req.params.id});
    const bool = notes[0].completed;
    await Note.updateOne(
        {userId: req.user._id, _id: req.params.id},
        {completed: !bool},
    );
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: error.message});
  }
}

/**
 * Delete Note
 * @param {obj} req
 * @param {obj} res
 */
async function deleteNote(req, res) {
  try {
    await Note.findOneAndDelete({
      _id: req.params.id,
      userId: req.user._id,
    });

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: error.message});
  }
}

module.exports = {
  createNote,
  getNotes,
  getNoteId,
  deleteNote,
  updateNote,
  modifyNote,
};
