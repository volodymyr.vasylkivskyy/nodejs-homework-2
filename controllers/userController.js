const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

/**
 * Get User Info
 * @param {object} req
 * @param {object} res
 */
async function getUser(req, res) {
  if (req.user) {
    res.status(200).json({user: req.user});
  } else {
    res.status(400).json({message: 'Authentification needed!'});
  }
}

/**
 * Delete User
 * @param {obj} req
 * @param {obj} res
 */
async function deleteUser(req, res) {
  try {
    await User.findOneAndDelete({username: req.user.username});
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: error.message});
  }
}

/**
 * Change Pass
 * @param {obj} req
 * @param {obj} res
 */
async function changePassword(req, res) {
  try {
    const user = await User.findOne({username: req.user.username});
    if (!(await bcrypt.compare(req.body.oldPassword, user.password))) {
      return res.status(400).json({message: `Wrong password!`});
    }

    await User.updateOne(
        {username: req.user.username},
        {password: await bcrypt.hash(req.body.newPassword, 10)},
    );

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: error.message});
  }
}

module.exports = {getUser, deleteUser, changePassword};
